var Calculator = {
    minimum: function (inputArray) {
        var minimum = inputArray[0];
        for (var i = 1; i < inputArray.length; i++) {
            if (minimum > inputArray[i]) {
                minimum = inputArray[i];
            }
        }
        return minimum;
    },
    maximum: function (inputArray) {
        var maximum = inputArray[0];
        for (var i = 1; i < inputArray.length; i++) {
            if (maximum < inputArray[i]) {
                maximum = inputArray[i];
            }
        }
        return maximum;
    },
    sum: function (inputArray) {
        var result = inputArray.reduce(function (head, tail) {
            return parseInt(head) + parseInt(tail);
        });
        return result;
    },
    average: function (inputArray) {
        var average = this.sum(inputArray) / inputArray.length;
        return average;
    },
    sequenceLength: function (inputArray) {
        return inputArray.length;
    }
};
