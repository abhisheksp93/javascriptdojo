var minListener = function () {
    document.getElementById("result-value").innerText = Calculator.minimum(extract());
};

var averageListener = function () {
    document.getElementById("result-value").innerText = Calculator.average(extract());
};

var maxListener = function () {
    document.getElementById("result-value").innerText = Calculator.maximum(extract());
};

var sequenceLengthListener = function () {
    document.getElementById("result-value").innerText = Calculator.sequenceLength(extract());
};

var addListeners = function () {
    document.getElementById("min").addEventListener("click", minListener);
    document.getElementById("max").addEventListener("click", maxListener);
    document.getElementById("average").addEventListener("click", averageListener);
    document.getElementById("length").addEventListener("click", sequenceLengthListener);
};

addListeners();