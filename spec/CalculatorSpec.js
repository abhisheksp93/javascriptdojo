describe("Calculator", function () {
    describe("Minimum", function () {
        it("should return 0 if array contains just 0", function () {
            expect(Calculator.minimum([0])).toBe(0);
        });
        it("should return the only number in the array", function () {
            expect(Calculator.minimum([1])).toBe(1);
        });
        it("should return minimum of 2 numbers in the array", function () {
            expect(Calculator.minimum([5, 1])).toBe(1);
        });
        it("should return minimum of 3 numbers in the array", function () {
            expect(Calculator.minimum([5, 1, 2])).toBe(1);
        });
    });

    describe("Maximum", function () {
        it("should return 0 if array contains just 0", function () {
            expect(Calculator.maximum([0])).toBe(0);
        });
        it("should return the only number in the array", function () {
            expect(Calculator.maximum([1])).toBe(1);
        });
        it("should return maximum of 2 numbers in the array", function () {
            expect(Calculator.maximum([5, 1])).toBe(5);
        });
        it("should return maximum of 3 numbers in the array", function () {
            expect(Calculator.maximum([-2, 1, 2])).toBe(2);
        });
    });

    describe("Sum", function () {
        it("should return 0 if array contains just 0", function () {
            expect(Calculator.sum([0])).toBe(0);
        });

        it("should sum of array", function () {
            expect(Calculator.sum([0])).toBe(0);
        });
    });

    describe("average", function () {
        it("should return 0 if array contains just 0", function () {
            expect(Calculator.average([0])).toBe(0);
        });
        it("should return average of array", function () {
            expect(Calculator.average([1, 5])).toBe(3);
        });
    });

    describe("sequenceLength", function () {
        it("should return 0 if array is empty", function () {
            expect(Calculator.sequenceLength([])).toBe(0);
        });
        it("should return 1 if array is contains only one element", function () {
            expect(Calculator.sequenceLength([10])).toBe(1);
        });
        it("should return 2 if array is contains only two elements", function () {
            expect(Calculator.sequenceLength([10, 20])).toBe(2);
        });
    });
});